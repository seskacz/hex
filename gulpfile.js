// Dependencies
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    prefix = require('gulp-autoprefixer');

// Sub tasks
gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss', {sourcemap: true, style: 'compact'})
        .pipe(sass().on('error', sass.logError))
        .pipe(prefix("last 4 version", "> 1%", "ie 8", "ie 7"))
        .pipe(sourcemaps.write('.'))

        .pipe(gulp.dest('./'));
});

gulp.task('watch', function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js-dev']);
});


gulp.task('js-vendor', function () {
    return gulp.src('./vendor/js/**/*.js')
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./js'));
});

gulp.task('css-vendor', function () {
    return gulp.src('./vendor/css/**/*.css')
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./css'));
});

gulp.task('js', function () {
   return gulp.src('./src/js/**/*.js')
       .pipe(concat('all.js'))
       .pipe(gulp.dest('./js'));
});

gulp.task('js-dev', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./js'));
});

gulp.task('assets', function () {
   return gulp.src('./src/assets/**/*')
       .pipe(gulp.dest('./assets'))
});

// Main tasks
gulp.task('default', ['sass', 'css-vendor', 'js-vendor', 'js', 'assets']);
gulp.task('dev', ['sass', 'watch', 'css-vendor', 'js-vendor', 'js-dev', 'assets']);
